#!/bin/bash
case $1 in
  init)
    /usr/bin/ssh-keygen -v -t dsa -f /etc/ssh/ssh_host_dsa_key -N ''
    /usr/bin/ssh-keygen -v -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''
    /usr/bin/ssh-keygen -v -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ''
    /usr/bin/ssh-keygen -v -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ''
    /usr/bin/ssh-keygen -v -A
    ;;
  start)
    echo "Starting ssh daemon..."
    /usr/sbin/sshd -D -p 2200
    ;;
esac
