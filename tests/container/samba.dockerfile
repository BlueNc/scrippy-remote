FROM alpine
ENV SMB_USER=luiggi.vercotti
ENV SMB_PASS=d34dp4rr0t

RUN apk update && \
    apk upgrade && \
    apk --update --no-cache add bash samba-common-tools samba-client samba-server supervisor && \
    mkdir -p /media/storage && \
    chmod 0777 /media/storage
RUN adduser -D -h "/home/${SMB_USER}" -s /bin/bash "${SMB_USER}" && \
    echo "${SMB_USER}:${SMB_PASS}" | /usr/sbin/chpasswd && \
    echo ${SMB_PASS} | tee - | smbpasswd -a -s "${SMB_USER}"
COPY smb.conf /etc/samba/smb.conf
COPY supervisord.conf /etc/supervisor/supervisord.conf

EXPOSE 445/tcp
EXPOSE 137/tcp
EXPOSE 138/tcp
EXPOSE 139/tcp

ENTRYPOINT ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
