FROM alpine:latest
RUN apk add openssh openssl shadow bash
COPY sshd_start.sh .
RUN chmod +x ./sshd_start.sh
RUN ./sshd_start.sh init
RUN useradd -m -d /home/scrippy -s /bin/bash scrippy
RUN usermod --password $(echo 0123456789 | openssl passwd -1 -stdin) scrippy
COPY ./scrippy/ /home/scrippy/
RUN chmod 700 /home/scrippy/.ssh
RUN chmod 640 /home/scrippy/.ssh/authorized_keys
RUN chown -R scrippy: /home/scrippy
USER root
ENTRYPOINT ["./sshd_start.sh", "start"]
EXPOSE 2200
