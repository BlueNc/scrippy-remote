FROM alpine
RUN apk update && \
    apk upgrade && \
    apk --update --no-cache add bash openssl vsftpd

RUN openssl req -x509 -nodes -days 7300 \
            -newkey rsa:2048 -keyout /etc/vsftpd/vsftpd.pem -out /etc/vsftpd/vsftpd.pem \
            -subj "/C=NC/O=Le Scrangourou/CN=Scrippy"

RUN mkdir -p /home/vsftpd/
RUN mkdir -p /var/log/vsftpd
RUN chown -R ftp:ftp /home/vsftpd/

COPY vsftpd.conf /etc/vsftpd/vsftpd.conf
COPY vsftpd_start.sh /usr/sbin/
RUN chmod +x /usr/sbin/vsftpd_start.sh

EXPOSE 2020-2021 2990 21100-21110

CMD /usr/sbin/vsftpd_start.sh
