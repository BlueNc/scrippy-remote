################################################################################
#  \033[93m Developper helpers:\n
#  \033[93m-------------------\n
#  \033[93m- Check code quality:\033[97m make lint\n
#  \033[93m- Code testing:\033[97m make tests\n
#  \033[93m- Update documentation:\033[97m make doc\n
#  \033[93m- Build python package:\033[97m make build\n
#  \033[93m- Install python package from source:\033[97m make install\n
#  \033[93m- Upgrade python package from source:\033[97m make upgrade\n
#  \033[93m- uninstall python package:\033[97m make uninstall\n
#  \033[93m- Clean repo before commit:\033[97m make distclean\n
#  \033[93m- Help:\033[97m make help \033[95m(show this help)\033[0m\n
#  \n
#  \033[93mRepo owner helpers:\n
#  \033[93m-------------------\n
#  \033[93m- Release:\033[97m make release \033[0m\n
################################################################################

.PHONY: release  build install uninstall upgrade upload distclean pytests gen_doc doc lint help

help:
	echo -e $$(grep ^\# Makefile | sed 's/\#//g')

release:
	git checkout master
	git pull origin master
	bump2version patch
	TAG=$$(grep version ./setup.cfg | cut -d\  -f3); \
	git tag "$${TAG}"; \
	git commit -am "Bump version: $${TAG} [CI SKIP]"; \
	git tag -d "$${TAG}"; \
	git add .; \
	git commit -am "Release: Version $${TAG} [CI SKIP]"; \
	git tag "$${TAG}"; \
	git push origin master --tags


build: distclean
	python3 -m build

install: distclean
	python3 ./setup.py build install --user

upload:
	twine upload  dist/*

uninstall:
	pip3 uninstall -y scrippy-remote

upgrade: uninstall install distclean

distclean:
	rm -Rf	./build \
					./dist \
					./src/scrippy-remote.egg-info \
					./tests/.pytest_cache \
					./junit_report.xml \
					./.coverage \
					./.pytest_cache
	-find ./ -type d -name "__pycache__" -exec rm -Rf {} \;
	-find ./ -type f -name "*.pyc" -exec rm -Rf {} \;

gen_doc:
	find ./docs/source -type f \( -name "*.rst" -and -not -name "index.rst" -and -not -name "modules.rst" \) -exec rm {} \;
	sphinx-apidoc -o ./docs/source ./src
	sphinx-build -M html ./docs/source ./docs/build
	pyreverse -o svg --colorized -p scrippy-remote -d ./specs/ -ASmy -f ALL ./src/scrippy_remote/

doc: | gen_doc distclean

pytests:
	python3 -m pytest -vv -x --cache-clear -s --cov-report=term --cov=scrippy_remote 2>/dev/null

tests: | pytests distclean

lint:
	pylama -i E111,E501 ./src/
