.. scrippy-remote documentation master file, created by
   sphinx-quickstart on Sat Dec 31 16:37:11 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to scrippy-remote's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
