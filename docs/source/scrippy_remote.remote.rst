scrippy\_remote.remote package
==============================

Submodules
----------

scrippy\_remote.remote.cifs module
----------------------------------

.. automodule:: scrippy_remote.remote.cifs
   :members:
   :undoc-members:
   :show-inheritance:

scrippy\_remote.remote.ftp module
---------------------------------

.. automodule:: scrippy_remote.remote.ftp
   :members:
   :undoc-members:
   :show-inheritance:

scrippy\_remote.remote.scrippy\_ftp module
------------------------------------------

.. automodule:: scrippy_remote.remote.scrippy_ftp
   :members:
   :undoc-members:
   :show-inheritance:

scrippy\_remote.remote.ssh module
---------------------------------

.. automodule:: scrippy_remote.remote.ssh
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: scrippy_remote.remote
   :members:
   :undoc-members:
   :show-inheritance:
